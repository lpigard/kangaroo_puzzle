#include "main.h"


int main(int argc, const char * argv[])
{
  int i;

  srand(time(NULL));

  for(i = 0; i < A0; i++)
  {
    NULL_PIECE.left[i] = 0;
    NULL_PIECE.right[i] = 0;
    NULL_PIECE.bot[i] = 0;
    NULL_PIECE.top[i] = 0;
  }
  NULL_PIECE.i_template = -1;
  NULL_PIECE.transform = -1;

  create_template_list();
  // save_template_list();

  random_permutation(template, N_TEMPLATES);

  find_possible_faces(possible_faces);

  return 0;
}

void find_possible_faces(piece possible[][DX][DY])
{
  int i, j;
  piece f[DX][DY];

  n_faces = 0;
  dead_ends = 0;
  n_trials = 0;

  make_face_empty(f);
  add_site(f, 0);

  FILE *file;
  char fname[1024];
  int x, y, r[AT0];

  for(i = 0; i < n_faces; i++)
  {
    for(x = 0; x < DX; x++)
    {
      for(y = 0; y < DY; y++)
      {
        sprintf(fname, "face_%d_%d_%d.dat", i, x, y);
        file = fopen(fname, "w");
        get_raw_piece(r, &possible_faces[i][x][y]);
        fprintf(file, "%d %d ", possible_faces[i][x][y].i_template, possible_faces[i][x][y].transform);
        for(j = 0; j < AT0 - 2; j++)
        {
          fprintf(file, "%d ", 0);
        }
        fprintf(file, "\n");
        for(j = 0; j < AT0; j++)
        {
          fprintf(file, "%d ", r[j]);
        }
        fclose(file);
      }
    }
  }

  printf("number of faces = %d; number of dead_ends = %ld; number of trials (try different piece for a site) = %ld\n", n_faces, dead_ends - n_faces, n_trials);
}

int add_site(piece f[DX][DY], int s)
{
  int i, t, x, y;

  if(s > s_max)
  {
    s_max = s;
    printf("s_max = %d\n", s_max);
  }

  x = s / DY;
  y = s % DY;

  if(s == DX * DY)
  {
    save_face_to_list(f, possible_faces, n_faces);
    n_faces++;
    printf("n_faces = %d\n", n_faces);
    if(n_faces >= N_FACES_MAX)
    {
      return 0;
    }
    else
    {
      return -1;
    }
  }

  for(i = 0; i < N_TEMPLATES; i++)
  {
    if(piece_available(template[i].i_template, f) == 1)
    {
      if(n_trials % BIG == 0)
      {
        if(n_trials > 0)
        {
          end = time(NULL);
          printf("%d | %ldE9 | %.2f\n", s, n_trials / BIG, (double)(end - start));
          end = start;
          start = time(NULL);
        }
        else
        {
          start = time(NULL);
        }
      }
      n_trials++;
      copy_piece(&template[i], &f[x][y]);
      if(template[i].i_template < 10) // if it is a piece with a kangoroo
      {
        for(t = 0; t < 1; t++)
        {
          transform_piece(&f[x][y]);
          if(is_piece_matching(f, x, y) == 1)
          {
            if(add_site(f, s + 1) == 0)
            {
              return 0;
            }
          }
        }
      }
      else
      {
        for(t = 0; t < 8; t++)
        {
          transform_piece(&f[x][y]);
          if(is_piece_matching(f, x, y) == 1)
          {
            if(add_site(f, s + 1) == 0)
            {
              return 0;
            }
          }
        }
      }
    }
  }

  dead_ends++;
  copy_piece(&NULL_PIECE, &f[x][y]);
  return -1;
}

void random_permutation(piece *r, int n)
{
  int i, j;
  piece temp;
  for(i = n - 1; i >= 0; --i)
  {
    //generate a random number [0, n-1]
    j = rand() % (i + 1);

    //swap the last element with element at random index
    copy_piece(&r[i], &temp);
    copy_piece(&r[j], &r[i]);
    copy_piece(&temp, &r[j]);
  }
}

int is_piece_matching(piece f[DX][DY], int x, int y)
{
  int cx, cy, four_occupied, dx, dy, xn, yn, dcx, dcy;

  // if(f[x][y].i_template < 9)
  // {
  //   if(y != 2)
  //   {
  //     return 0;
  //   }
  // }
  // else
  // {
  //   if(y == 2)
  //   {
  //     return 0;
  //   }
  // }

  // check if sides with neighboring sites match
  xn = x - 1;
  yn = y;

  if(xn >= 0 && f[xn][yn].i_template != -1 && matching_left_right(&f[xn][yn], &f[x][y]) == 0)
  {
    return 0;
  }

  xn = x + 1;
  yn = y;
  if(xn < DX && f[xn][yn].i_template != -1 &&  matching_left_right(&f[x][y], &f[xn][yn]) == 0)
  {
    return 0;
  }

  xn = x;
  yn = y - 1;
  if(yn >= 0 && f[xn][yn].i_template != -1 && matching_bot_top(&f[xn][yn], &f[x][y]) == 0)
  {
    return 0;
  }

  xn = x;
  yn = y + 1;
  if(yn < DY && f[xn][yn].i_template != -1 && matching_bot_top(&f[x][y], &f[xn][yn]) == 0)
  {
    return 0;
  }

  // check if corners with neighboring sites match
  for(dcx = 0; dcx <= 1; dcx++)
  {
    for(dcy = 0; dcy <= 1; dcy++)
    {
      cx = x + dcx;
      cy = y + dcy;
      four_occupied = 1;
      for(dx = 0; dx <= 1; dx++)
      {
        for(dy = 0; dy <= 1; dy++)
        {
          xn = cx - dx;
          yn = cy - dy;
          if(xn < 0 || xn >= DX || yn < 0 || yn >= DY || f[xn][yn].i_template == -1) // out of boundaries or empty site
          {
            four_occupied = 0;
          }
        }
      }
      if(four_occupied == 1)
      {
        if(cx < 0 || cx >= DX || cx - 1 < 0 || cx - 1 >= DX || cy < 0 || cy >= DY || cy - 1 < 0 || cy - 1 >= DY)
        {
          printf("ERROR\n");
          return -1;
        }
        if(f[cx - 1][cy - 1].right[0] + f[cx][cy - 1].top[0] + f[cx - 1][cy].right[4] + f[cx][cy].bot[0] != 1)
        {
          return 0;
        }
      }
    }
  }

  return 1;
}

void save_face_to_list(piece f[DX][DY], piece l[][DX][DY], int i)
{
  int x, y;

  for(x = 0; x < DX; x++)
  {
    for(y = 0; y < DY; y++)
    {
      copy_piece(&f[x][y], &l[i][x][y]);
    }
  }
}

int piece_available(int i, piece f[DX][DY])
{
  int x, y;

  for(x = 0; x < DX; x++)
  {
    for(y = 0; y < DY; y++)
    {
      if(i == f[x][y].i_template)
      {
        return 0;
      }
    }
  }

  return 1;
}

void make_face_empty(piece f[DX][DY])
{
  int x, y;

  for(x = 0; x < DX; x++)
  {
    for(y = 0; y < DY; y++)
    {
      copy_piece(&NULL_PIECE, &f[x][y]);
    }
  }
}


int matching_left_right(piece *p1, piece *p2)
{
  int i;

  if((*p1).right[0] + (*p2).left[0] > 1)
  {
    return 0;
  }
  if((*p1).right[A0 - 1] + (*p2).left[A0 - 1] > 1)
  {
    return 0;
  }

  for(i = 1; i < A0 - 1; i++)
  {
    if((*p1).right[i] + (*p2).left[i] != 1)
    {
      return 0;
    }
  }

  return 1;
}

int matching_bot_top(piece *p1, piece *p2)
{
  int i;

  if((*p1).top[0] + (*p2).bot[0] > 1)
  {
    return 0;
  }

  if((*p1).top[A0 - 1] + (*p2).bot[A0 - 1] > 1)
  {
    return 0;
  }

  for(i = 1; i < A0 - 1; i++)
  {
    if((*p1).top[i] + (*p2).bot[i] != 1)
    {
      return 0;
    }
  }

  return 1;
}

void transform_piece(piece *p)
{
  piece copy;
  int i;

  if((*p).transform == -1){
    (*p).transform = 0;
  }
  else
  {
    copy_piece(p, &copy);

    if((*p).transform == 3) // flip piece
    {
      for(i = 0; i < A0; i++)
      {
        (*p).left[i] = copy.right[i];
        (*p).right[i] = copy.left[i];
        (*p).top[i] = copy.top[A0 - i - 1];
        (*p).bot[i] = copy.bot[A0 - i - 1];
      }
    }
    else // rotate piece by 90 degrees
    {
      for(i = 0; i < A0; i++)
      {
        (*p).top[i] = copy.left[A0 - i - 1];
        (*p).right[i] = copy.top[i];
        (*p).bot[i] = copy.right[A0 - i - 1];
        (*p).left[i] = copy.bot[i];
      }
    }
    (*p).transform++;
  }

}

void copy_piece(piece *src, piece *dst)
{
  int i;

  for(i = 0; i < A0; i++)
  {
    (*dst).left[i] = (*src).left[i];
    (*dst).right[i] = (*src).right[i];
    (*dst).bot[i] = (*src).bot[i];
    (*dst).top[i] = (*src).top[i];
  }

  (*dst).i_template = (*src).i_template;
  (*dst).transform = (*src).transform;
}

void create_template_list()
{
  int i;

  for(i = 0; i < N_TEMPLATES; i++)
  {
    template[i].left[0] = raw_template[i][0];
    template[i].left[1] = raw_template[i][15];
    template[i].left[2] = raw_template[i][14];
    template[i].left[3] = raw_template[i][13];
    template[i].left[4] = raw_template[i][12];

    template[i].right[0] = raw_template[i][4];
    template[i].right[1] = raw_template[i][5];
    template[i].right[2] = raw_template[i][6];
    template[i].right[3] = raw_template[i][7];
    template[i].right[4] = raw_template[i][8];

    template[i].top[0] = raw_template[i][0];
    template[i].top[1] = raw_template[i][1];
    template[i].top[2] = raw_template[i][2];
    template[i].top[3] = raw_template[i][3];
    template[i].top[4] = raw_template[i][4];

    template[i].bot[0] = raw_template[i][12];
    template[i].bot[1] = raw_template[i][11];
    template[i].bot[2] = raw_template[i][10];
    template[i].bot[3] = raw_template[i][9];
    template[i].bot[4] = raw_template[i][8];

    template[i].i_template = i;
    template[i].transform = -1;
  }
}

void save_template_list()
{
  int i, j, r[AT0];
  FILE *f = fopen("templates.dat", "w");

  for(i = 0; i < N_TEMPLATES; i++)
  {
    get_raw_piece(r, &template[i]);

    for(j = 0; j < AT0; j++)
    {
      fprintf(f, "%d ", r[j]);
    }
    fprintf(f, "\n");
  }

  fclose(f);
}

void get_raw_piece(int r[AT0], piece *p)
{
  r[0] = (*p).top[0];
  r[1] = (*p).top[1];
  r[2] = (*p).top[2];
  r[3] = (*p).top[3];
  r[4] = (*p).top[4];
  r[5] = (*p).right[1];
  r[6] = (*p).right[2];
  r[7] = (*p).right[3];
  r[8] = (*p).right[4];
  r[9] = (*p).bot[3];
  r[10] = (*p).bot[2];
  r[11] = (*p).bot[1];
  r[12] = (*p).bot[0];
  r[13] = (*p).left[3];
  r[14] = (*p).left[2];
  r[15] = (*p).left[1];
}
