#!/usr/bin/env python

import numpy as np
import sys
import h5py
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
# from matplotlib import ticker

DX = 9
DY = 6
n_faces = 1

def calc_xy(j):
    if j == 0:
        return [0, 4]
    if j == 1:
        return [1, 4]
    if j == 2:
        return [2, 4]
    if j == 3:
        return [3, 4]
    if j == 4:
        return [4, 4]
    if j == 5:
        return [4, 3]
    if j == 6:
        return [4, 2]
    if j == 7:
        return [4, 1]
    if j == 8:
        return [4, 0]
    if j == 9:
        return [3, 0]
    if j == 10:
        return [2, 0]
    if j == 11:
        return [1, 0]
    if j == 12:
        return [0, 0]
    if j == 13:
        return [0, 1]
    if j == 14:
        return [0, 2]
    if j == 15:
        return [0, 3]
    print('incorrect index')
    return -1

def raw_2_real(r):
    xy = np.zeros((5, 5)) + 1
    for i in range(len(r)):
        if(r[i] == 0):
            c = calc_xy(i)
            xy[c[0], c[1]] = 0
    return xy


templates = np.loadtxt('templates.dat', dtype='i')

nt = np.size(templates, 0)
print('number of templates = {}'.format(nt))
for i in range(nt):
    t = templates[i, :]
    xy = raw_2_real(t)
    plt.imshow(xy.T, interpolation='none', origin='lower', cmap = 'Greys')
    plt.axis('off')

    # plt.show()
    plt.savefig('template_{}.png'.format(i))
    plt.close()

k = np.zeros((10, 2))
fdis = np.zeros(n_faces)

for f in range(n_faces):
    for x in range(DX):
        for y in range(DY):
            data = np.loadtxt('face_{}_{}_{}.dat'.format(f, x, y), dtype='i')
            i_temp = data[0][0]
            i_trafo = data[0][1]
            data = data[1][:]
            xy = raw_2_real(data)
            plt.subplot(DY, DX, 1 + x + (DY - y - 1) * DX).set_title('{} ({})'.format(i_temp, i_trafo))
            c = 'Reds' if i_temp < 10 else 'Greys'
            plt.imshow(xy.T, interpolation='none', origin='lower', cmap = c)
            plt.axis('off')

            if i_temp < 10:
                k[i_temp][0] = x
                k[i_temp][1] = y
    plt.tight_layout()
    print('save face {} / {}'.format(f + 1, n_faces))
    plt.savefig('face_{}.png'.format(f))
    plt.close()

    dis = 0
    for i in range(10):
        for j in range(10):
            dis += np.sqrt((k[i][0] - k[j][0])**2 + (k[i][1] - k[j][1])**2)
    fdis[f] = dis / 100

print(np.argmax(fdis), np.amax(fdis))
